Router.configure({
	layoutTemplate: 'layout',
	notFoundTemplate: 'errorpage'
});

Router.map(function () {
	this.route('home', 	{ path:'/' });
	this.route('generator');
	// this.route('errorpage', { path: '*' });
});